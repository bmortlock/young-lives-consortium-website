# Generated by Django 3.0.6 on 2020-06-14 17:59

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailforms', '0004_add_verbose_name_plural'),
        ('contenttypes', '0002_remove_content_type_name'),
        ('wagtailcore', '0045_assign_unlock_grouppagepermission'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('wagtailimages', '0022_uploadedimage'),
        ('wagtailredirects', '0006_redirect_increase_max_length'),
        ('home', '0012_auto_20200614_1842'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='ContentPage',
            new_name='EventPage',
        ),
    ]
