from django.db import models

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.images.models import Image
from wagtail.images.edit_handlers import ImageChooserPanel

footer = models.ForeignKey('wagtailimages.Image',
                           null=True,
                           blank=True,
                           on_delete=models.SET_NULL,
                           related_name='+'),
# Create the panels
content_panels = Page.content_panels + [
    FieldPanel('heading', classname="full"),
    FieldPanel('body', classname="full"),
    ImageChooserPanel('Footer'),
    ]
pass

class HomePage(Page):

    # define basic text field
    heading = models.TextField(blank=True)

    # define a rich text field
    body = RichTextField(blank=True)

    textbox1 = RichTextField(blank=True)
    textbox2 = RichTextField(blank=True)
    textbox3 = RichTextField(blank=True)
    textbox4 = RichTextField(blank=True)

    # define image field
    footer = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+')

    logo = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+')


    homeimg1 = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+')

    homeimg2 = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+')

    homeimg3 = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+')

    homeimg4 = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+')

    # create the panels
    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
        FieldPanel('textbox1', classname="full"),
        FieldPanel('textbox2', classname="full"),
        FieldPanel('textbox3', classname="full"),
        FieldPanel('textbox4', classname="full"),
        FieldPanel('heading', classname="full"),
        ImageChooserPanel('footer'),
        ImageChooserPanel('logo'),
        ImageChooserPanel('homeimg1'),
        ImageChooserPanel('homeimg2'),
        ImageChooserPanel('homeimg3'),
        ImageChooserPanel('homeimg4'),

    ]
    pass

class EventPage(Page):

    # define basic text field
    heading = models.TextField(blank=True)

    # define a rich text field
    body = RichTextField(blank=True)

    # define image field
    footer = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+')

    logo = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+')

    img1 = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+')

    img2 = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+')

    eventcalendar = RichTextField(blank=True)


    # create the panels
    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
        FieldPanel('eventcalendar', classname="full"),
        FieldPanel('heading', classname="full"),
        ImageChooserPanel('footer'),
        ImageChooserPanel('logo'),
        ImageChooserPanel('img1'),
        ImageChooserPanel('img2'),

    ]
    pass

